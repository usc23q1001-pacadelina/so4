class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    
    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program.")
    
    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")
        
s04_camper = Camper("John", "Batch 1", "Full Stack Web Development")

print(f"Name: {s04_camper.name}")
print(f"Batch: {s04_camper.batch}")
print(f"Course Type: {s04_camper.course_type}")

s04_camper.info()
s04_camper.career_track()
